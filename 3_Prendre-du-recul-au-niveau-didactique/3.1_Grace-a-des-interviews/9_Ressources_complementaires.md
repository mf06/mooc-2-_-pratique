## De la recherche internationale en didactique de l'informatique

Au niveau international l'[Association for Computing Machinery's](https://en.wikipedia.org/wiki/Association_for_Computing_Machinery) (ACM) Special Interest Group (SIG) on [Computer science education](https://en.wikipedia.org/wiki/Computer_science_education) (CSE), rassemble au sein de la [SIGCSE](https://en.wikipedia.org/wiki/SIGCSE) les initiatives de recherche, développement, implémentation, et évaluation des ressources pour l'apprentissage de l'informatique.


Sur le forum NSI [Louis Leskow](https://mooc-forums.inria.fr/moocnsi/u/louis_leskow) nous propose une sélection :

- Des idées de thèmes d’exercices: https://dl.acm.org/doi/10.1145/3341525.3387427
- De nouvelles formes d’exercices: https://dl.acm.org/doi/10.1145/3141880.3141895
- Des exemples de curriculum en pratique avec retours: https://dl.acm.org/doi/10.1145/3287324.3287428
- Des choses générales sur les difficultés des apprenants: https://dl.acm.org/doi/10.1145/572139.572181
- Des analyses sur des points particuliers: https://dl.acm.org/doi/10.1145/2960310.2960327
- Des articles sur ce qui peut aider les filles à s’intéresser et à réussir: https://dl.acm.org/doi/10.1145/2960310.2960329

Il mentionne aussi [bluej.org/blackbox](https://bluej.org/blackbox) qui est une de partage de données sur les pratiques des élèves/étudiant·e·s.

Nous pourrions paratger sur ces sujets sur le [forum NSI](https://mooc-forums.inria.fr/moocnsi/t/ressources-pour-la-didactique-de-linformatique/2840)

### Un recueil de ressources sur la didactique de l'informatique

 À l'initiative de [Julie Henry](https://researchportal.unamur.be/fr/persons/julie-henry), voici un recueil de ressources didactiques co-construit par les collègues … profitons-en et … participons y !

 - [Aspects didactiques de l'informatique](https://docs.google.com/document/d/1c_NxE4HONPSxkLJpMmmdKg9RUiB7zpQpbNvzxUPNHHQ/edit)
