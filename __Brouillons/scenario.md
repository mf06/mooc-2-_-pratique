# Scénario MOOC 2

## Module Introduction

1. Séquence Introduction
2. Séquence Interview de D. Roche
    
    J'imagine plusieurs unités : 1 par question ? qui pourrait être le titre de l'unité
3. Séquence Interview de T. Lelievre-Osswald

## Penser, Concevoir, Elaborer

1. Utiliser

    - Unité 1 : la fiche exemple de Maxime (le fichier Introduction.md)
    - Unité 2 un petit quiz ?
    - Unité 3 : l'Activité 01
    - Unité 4 : l'Activité 02

2. Créer 

## Mettre en oeuvre, Animer

## Accompagner

## Observer, Analyse, Evaluer