# Fiche thématique

## Activité 3 : remédiation

A la  suite d’une évaluation formative sur les requêtes  SQL, vous  constatez que certains élèves éprouvent des difficultés avec les jointures (utilisation de ‘INNER JOIN’). Proposer une remédiation afin de corriger ce problème.
