Dans ce MOOC vous allez naviguer entre production de ressources et prises de reculs didactiques et pédagogiques.

Dans ce premier bloc, nous vous demandons de :

- **Commencer par parler de didactique**. Vous êtes invité·e·s à faire connaissance avec Tessa et David, deux enseignant·e·s de NSI qui se sont prêtés au jeu des questions-réponses. A la fin de ce bloc vous pourrez également vous aussi vous préter au jeu des questions-réponses en échangeant avec la communauté NSI.

- **Préparer votre espace de travail** en suivant les tutoriels qui concernent la plateforme Gitlab, le langage Markdown et la découverte de deux exerciseurs de notebooks : Basthon et Capytale. Des petits exercices de prise en main vous permettront de prendre vos marques.

- Commencer à lire le début d'un **ouvrage de didactique dirigé par Werner Hartmann et intitulé _Enseigner l'Informatique_**, ensuite nous vous invitons à répondre au quiz qui cloturera ce bloc.

Bonne découverte!
